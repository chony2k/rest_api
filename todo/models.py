# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    """
    Task model. Represents a task in a To-Do application.
    """

    TASK_STATUS_UNASSIGNED = 1
    TASK_STATUS_PENDING = 2
    TASK_STATUS_EXECUTING = 3
    TASK_STATUS_DONE = 4
    TASK_STATUS_ABORTED = 5

    TASK_STATUS_CHOICES = (
        (TASK_STATUS_UNASSIGNED, 'unassigned'),
        (TASK_STATUS_PENDING, 'pending'),
        (TASK_STATUS_EXECUTING, 'executing'),
        (TASK_STATUS_DONE, 'done'),
        (TASK_STATUS_ABORTED, 'aborted')
    )

    assignee = models.ForeignKey(User)
    title = models.CharField(max_length=50, blank=False, null=False)
    description = models.CharField(max_length=500, blank=False, null=False)
    status = models.IntegerField(choices=TASK_STATUS_CHOICES, default=TASK_STATUS_UNASSIGNED, blank=False, null=False)

    def __unicode__(self):
        return self.title
