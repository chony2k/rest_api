# -*- coding: utf-8 -*-

import logging

from django.core.exceptions import ObjectDoesNotExist

from django.contrib.auth.models import User
from rest_framework import generics, status
from rest_framework.views import APIView
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.permissions import IsAuthenticated

from .serializers import UserSerializer, TaskSerializer
from todo.models import Task

logger = logging.getLogger(__name__)


class APIRoot(APIView):
    """
    To-Do API root
    """

    def get(self, request):
        return Response({
            'task_list': reverse('api_task_list', request=request),
            'task_create': reverse('api_task_create', request=request),
            'task_complete': reverse('api_task_complete', request=request, kwargs={'pk': 1}),
            'user_create': reverse('api_user_create', request=request)
        })


class UserCreate(generics.CreateAPIView):
    """
    API endpoint that allows users to be created.
    """
    model = User
    serializer_class = UserSerializer


class TaskList(generics.ListAPIView):
    """
    API endpoint that returns all tasks
    """
    model = Task
    serializer_class = TaskSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        Query returns all tasks
        """
        return Task.objects.all()


class TaskCreate(generics.CreateAPIView):
    """
    API endpoint that creates a Task
    """
    model = Task
    serializer_class = TaskSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        try:
            user = User.objects.get(id=request.data.get('assignee', None))
        except ObjectDoesNotExist:
            msg = 'User with id: %s does not exist' % request.data.get('assignee', None)
            logger.error(msg)
            raise NotFound(msg)

        return super(TaskCreate, self).post(request, *args, **kwargs)


class TaskComplete(generics.GenericAPIView):
    """
    API endpoint that updates a task, setting it's 'status' field to 'done'
    The method expects a POST request where the resource URL contains the id (pk) of the task to be updated

    if the task can't be found, a standard 404 HTTP error code is returned. If the call succeeds
    the message 'task_status' is returned to the client, with the new task status code.
    """
    model = Task
    serializer_class = TaskSerializer
    permission_classes = (IsAuthenticated,)
    allowed_methods = ['post']

    def get_object(self, queryset=None):
        """
        Get task instance given the 'pk' keyword argument
        :param queryset:
        :return: Task object
        """
        try:
            return Task.objects.get(pk=self.kwargs['pk'])
        except ObjectDoesNotExist:
            msg = 'Task with id: %s does not exist' % self.kwargs['pk']
            logger.error(msg)
            raise NotFound(msg)

    def post(self, request, *args, **kwargs):
        task = self.get_object()
        task.status = Task.TASK_STATUS_DONE
        task.save()
        return Response({'task_status', Task.TASK_STATUS_DONE}, status=status.HTTP_200_OK)
