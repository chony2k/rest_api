from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from todo.models import Task


class TaskTests(APITestCase):
    """
    Task tests
    """
    user = None
    factory = APIRequestFactory()

    def setUp(self):
        """
        Set up tests
        Creating one initial task and test user
        """

        # Creating a test user
        self.user = User.objects.create(username="testuser", email="testuser@test.com", password="secret")

        # Creating a test task which belongs to the test user
        Task.objects.create(title="first", description="description", status=Task.TASK_STATUS_UNASSIGNED,
                            assignee=User.objects.get(username="testuser"))

        # Creating a token for the test user
        Token.objects.create(user=self.user)

    def test_api_task_list(self):
        """
        Ensure that tasks are returned successfully. The test takes into account authentication
        """
        url = reverse('api_task_list')

        token = Token.objects.get(user__username='testuser')
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        response = client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Task.objects.count(), 1)
        self.assertEqual(Task.objects.get().title, 'first')

    def test_api_task_create(self):
        """
        Ensure we can create a new task object. The test takes into account authentication
        """
        url = reverse('api_task_create')

        data = {
            'title': 'second',
            'description': 'description',
            'status': Task.TASK_STATUS_UNASSIGNED,
            'assignee': 1
        }

        token = Token.objects.get(user__username='testuser')
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        response = client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsInstance(Task.objects.get(title='second'), Task)

    def test_api_task_complete(self):
        """
        Ensure we can update a task.
        """
        url = reverse('api_task_complete', kwargs={'pk': 1})

        token = Token.objects.get(user__username='testuser')
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        response = client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Task.objects.get(pk=1).status, Task.TASK_STATUS_DONE)
        self.assertEqual(response.data, {'task_status', Task.TASK_STATUS_DONE})


class UserTests(APITestCase):
    """
    User tests
    """

    def test_api_user_create(self):
        """
        Ensure we can create a user. This method is intentionally left anonymous so that unauthenticated clients can
        register accounts. Typically these accounts would be verified and activated, before generating a token for them,
        and allow them to log into the system.
        :return:
        """

        url = reverse('api_user_create')
        data = {
            'username': 'test',
            'email': 'test@test.com',
            'password': 'secret'
        }
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsInstance(User.objects.get(username='test'), User)
