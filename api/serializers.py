# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from rest_framework import serializers

from todo.models import Task


class UserSerializer(serializers.ModelSerializer):
    """
    User Serializer
    """

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class TaskSerializer(serializers.ModelSerializer):
    """
    Task Serializer
    """

    class Meta:
        model = Task
        fields = ('assignee', 'title', 'description', 'status')
