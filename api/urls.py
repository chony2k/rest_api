from django.conf.urls import url
from rest_framework.authtoken import views

from .views import *

urlpatterns = [
    url(r'^$', APIRoot.as_view(), name='api_root'),
    url(r'^tasks/list/$', TaskList.as_view(), name='api_task_list'),
    url(r'^tasks/create/$', TaskCreate.as_view(), name='api_task_create'),
    url(r'^tasks/(?P<pk>[0-9]+)/complete/$', TaskComplete.as_view(), name='api_task_complete'),
    url(r'^users/register/$', UserCreate.as_view(), name='api_user_create'),
    url(r'^api-token-auth/', views.obtain_auth_token),
]
