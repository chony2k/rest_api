# API #
---

Se dividió el proyecto en 3 aplicaciones:

    - api: Contiene la implementación de los endpoints de la API
    - todo: Contiene la lógica de negocio de la aplicación de To-Do.
      En este caso es solamente el modelo Task, que representa a una tarea
    - main: Contiene configuración general


Adicionalmente, se hicieron pruebas para la API, y se brindan los archivos requirements.txt y circle.yml para la
integración con CircleCI y la instalación de dependencias en un virtual enviroment, respectivamente.

A continuación una breve descripción de cada endpoint:

**/api/users/register**

    Espera username, email y password. Con estos valores crea un usuario. No requiere autenticación, pues probablemente
    este endpoint se use por clientes anónimos para registrar un usuario nuevo. En producción se debe esperar a que el
    usuario se loguee al sitio y verifique su email para activar la cuenta.

**/api/tasks/lists**

    Devuelve las tares actuales. Requiere autenticación.

**/api/tasks/create**

    Espera title, description, assignee y status. Con estos valores crea una tarea. 'assignee' es el usuario al cual
    Se le asigna la tarea, debe existir previamente. Este método requiere autenticación.

**/api/tasks/{pk}/complete**

    Actualiza una tarea identificada por {pk} modificando su estado a 'done'. La tarea debe existir previamente.
    El método devuelve el id del nuevo estado.

**/api-token-auth**

    Espera un POST con username y password, con esto devuelve un token.

**/docs**

    Documentación brindada por rest_framework_swagger


# Instalación: #

1. Clonar repo
2. Crear virtual environment
3. Instalar requerimientos: pip install -r requirements.txt
4. Ya está una db inicial creada en el repo, si se quiere empezar de cero, eliminarla, y...  ./manage.py syncdb
5. ./manage.py runserver


# Notas: #

  *  Se separó la URL de /api-token-auth del resto de las URLs de la API, de forma que se pueda cambiar el mecanismo
     de autenticación sin tocar la API. Asismismo, se separó la aplicación del modelo de Tarea de la API de forma que
     sea reutilizable en otros entornos no web.

  *  El requerimiento de 'Registrar un usuario' se asumió como 'crear' un usuario. En caso de que quisiera decir
     loguear, se brinda la URL api-token-auth  para obtener un token.

  *  El requerimiento de 'Marcar una tarea como resuelta' se asumió como cambiar solamente se valor (status).

  *  El requerimiento de 'Obtener la lista de tareas y su estado actual' se asumió como que las tareas ya traen su estado.
     Los clientes podrían tener los estados posibles (los Ids) y su representación textual (nombre descriptivo)
     de antemano. Para esto se puede hacer otro endpoint, pero en aras de simplicidd y tiempo, lo dejamos así.

  *  Se usó *tokenauthentication* para la autenticación. Para las cuentas creadas a través de la API, no se genera token,
     intencionalmente, de forma que sea necesario un mecanismo de activación de la cuenta.

  * Se utilizó idioma Inglés para el código y comentarios