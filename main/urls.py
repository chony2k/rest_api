from django.contrib import admin
from django.conf.urls import include, url
from rest_framework.authtoken import views

admin.autodiscover()

urlpatterns = [
    url(r'^api/', include('api.urls')),
    url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^docs/', include('rest_framework_swagger.urls')),
]
